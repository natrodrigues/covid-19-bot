# Estratégias para testes

De acordo com o [diagrama](https://drive.google.com/file/d/1t67Atdq32e8gMtYqBQoBATMr51UNVTSg/view) de componentes e suas conexões, os testes serão feios a nível de componentes, integração dos mesmos e, por fim, testes a nível de sistema quando o fluxo de comentários provenientes do Twitter forem processados e armazenados no banco de dados, bem como a integração deste com a interface gráfica.

## Teste de componentes

- ### Sistema de classificação de componentes
[teste de unidade, teste de funcionalidade, teste de caixa preta]

- ### Processamento de Linguagem
[teste de unidade, teste de funcionalidade, teste de caixa preta]

[teste de unidade, teste de funcionalidade, teste de caixa branca]

- ### Busca de comentários
[teste de unidade, teste de funcionalidade, teste de caixa preta]

- ### Repositórios de comentários
[teste de unidade, teste de funcionalidade, teste de caixa preta]

[teste de unidade, teste de segurança, teste de caixa branca]

- ### Gerenciador de repositório
[teste de unidade, teste de funcionalidade, teste de caixa preta]

- ### Controlador
[teste de unidade, teste de funcionalidade, teste de caixa preta]

[teste de unidade, teste de robustez, teste de caixa branca]

- ### GUI
[teste de unidade, teste de funcionalidade, teste de caixa preta]

[teste de unidade, teste de robustez, teste de caixa branca]

[teste de unidade, teste de usabilidade, teste de caixa preta]


## Teste de integracão

- ### Busca de comentários e seus subscribers
[teste de integridade, teste de funcionalidade, teste de caixa preta]

[teste de integridade, teste de robustez, teste de caixa preta]

- ### Repositórios de comentários e suas conexões externas
[teste de integridade, teste de funcionalidade, teste de caixa preta]

[teste de integridade, teste de segurança, teste de caixa branca]

- ### Controlador, Visão e Gerenciador
[teste de integridade, teste de funcionalidade, teste de caixa preta]

[teste de integridade, teste de robustez, teste de caixa preta]


## Teste de sistema

- ### Busca comentários até repositório de comentários classificados
[teste de sistema, teste de funcionalidade, teste de caixa preta]

[teste de integridade, teste de robustez, teste de caixa preta]

- ### GUI até repositório de comentários classificados
[teste de sistema, teste de funcionalidade, teste de caixa preta]

[teste de integridade, teste de robustez, teste de caixa preta]

- ### Todos os componentes e suas conexões
[teste de sistema, teste de funcionalidade, teste de caixa preta]

[teste de integridade, teste de robustez, teste de caixa preta]