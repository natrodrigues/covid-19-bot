import psycopg2

class CommentDao:

    self.__table = 'Tweets'

    def __init__(self):
        self.__conn = psycopg2.connect(host='nami.cm9psukgluoa.us-east-2.rds.amazonaws.com', 
            database='postgres', user='postgres', password='boradale123', port=5432)
        self.__cur = self.__conn.cursor()

    def save(self, comms: list):
        for comm in comms:
            self.__save_in_db(comm)

    def __save_in_db(self, comm):
        sql = 'INSERT INTO {0} (text, classification) VALUES({1}, {2})'.format(self.__table, comm['text'], comm['classification'])
        self.__cur.execute(sql)
        self.__cur.commit()

    def fetch(self):
        return self.__cur.fetchall()
    
    def close(self):
        self.__cur.close()
        self.__conn.close()