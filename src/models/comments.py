from nltk.tokenize import word_tokenize


class Comment:
    def __init__(self, comments: dict):
        self.__comment = comments

    # Comentário recém buscado não possui label
    def get_labels(self):
        # Crítica positiva ou negativa
        return self.__comment['classification']

    def get_broken_comments(self):
        if not isinstance(self.__comment['text'], list):
            if isinstance(self.__comment['text'], str):
                self.__comment['text'] = [self.__comment['text']]
            else:
                raise Exception()
        comment_phrases = [comm.lower() for comm in self.__comment['text']]
        broken_comments = [word_tokenize(comment_phrase)
                         for comment_phrase in comment_phrases]
        return broken_comments

    def get_comment(self):
        return self.__comment

    def __str__(self):
        return self.__comment.__str__()

