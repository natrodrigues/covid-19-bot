from joblib import load

import tweepy

keys = load('keys.tt')

auth = tweepy.OAuthHandler(keys['CONSUMER_KEY'], keys['CONSUMER_SECRET'])
auth.set_access_token(keys['ACCESS_TOKEN'], keys['ACCESS_TOKEN_SECRET'])

api = tweepy.API(auth)

# updated_status =api.update_status('Hello World') 

# print(updated_status)

# exit(1)

def get_since_id():
    return '1281381033317933056';

def get_max_id():
    pass

def get_count():
    pass

kwargs = {
    'since_id' : '1257182938304741377'
    # 'max_id' : get_max_id(),
    # 'count' : get_count(),
}

user_timeline = {
    'screen_name' : 'natrodrigues123',
    'since_id' : '1281381033317933056',
    'page' : 100,
}

# tl=  api.user_timeline(user_timeline)
# print(tl)

# home = api.user_timeline(kwargs)
# print(home)
status = api.mentions_timeline(kwargs)

print(status)