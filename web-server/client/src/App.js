import React, { useState, useEffect } from 'react';
import {BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Login from './views/Login/Login';
import Sidebar from './components/Sidebar/Sidebar';
import Positive from './views/Positive/Positive';
import Negative from './views/Negative/Negative';
import Percentage from './views/Percentage/Percentage';

import styles from './App.module.scss';

const App = () => {
  const [isAuth, setIsAuth]  = useState(false);

  useEffect(()=>{
    if(localStorage.getItem('isAuth') && (localStorage.getItem('isAuth') === true || localStorage.getItem('isAuth') === 'true')) {
      setIsAuth(true);
    }
    else{
      localStorage.setItem('isAuth', false);
       setIsAuth(false);
    }
  }, []);

  return (
    <div className={styles.App}>
      { isAuth ? 
        <Router>
          <Sidebar setIsAuth={setIsAuth}/>      
          <Switch>
            <Route exact path="/percentage" children={<Percentage />} />
            <Route path="/positive" children={<Positive />} />
            <Route path="/negative" children={<Negative />} />
          </Switch>
        </Router> 
      : <Login setIsAuth={setIsAuth} /> 
      }
    </div>
  );
  
}

export default App;