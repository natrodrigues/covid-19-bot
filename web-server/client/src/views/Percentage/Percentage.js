import React, {useState, useEffect} from "react";

import styles from "./Percentage.module.scss";

const Percentage = () => {
  const [pos, setPos] = useState(0);
  const [neg, setNeg] = useState(0);

  const fetchCount = async () => {
    const responseNeg = await fetch('/api/count/0', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        }
    });
    const bodyNeg = await responseNeg.text();
    const dataNeg = JSON.parse(bodyNeg);
    setNeg(parseInt(dataNeg.count, 10));
   
    const responsePos = await fetch('/api/count/1', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        }
    });
    const bodyPos = await responsePos.text();
    const dataPos = JSON.parse(bodyPos);
    setPos(parseInt(dataPos.count, 10));
  }

  useEffect(()=>{
    fetchCount();
  }, []);

  console.log('pos', pos)
  console.log('neg', neg)

  return (
    <div className={styles.percentage}>
      <div 
        className={styles.positive} 
        style={{width: `${((pos/(pos+neg))*100)}%`}} 
      >
        <span>Positivo ({Math.round(pos/(pos+neg)*100)}%)</span>
      </div>
      <div 
        className={styles.negative} 
        style={{width: `${((neg/(pos+neg))*100)}%`}} 
      >
        <span>Negativo ({Math.round(neg/(pos+neg)*100)}%)</span>
      </div>
    </div>
  );
};

export default Percentage;
