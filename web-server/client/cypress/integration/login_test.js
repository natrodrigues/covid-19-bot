const base_url = Cypress.config().baseUrl;
const username = Cypress.env('USERNAME');
const password = Cypress.env('PASSWORD');

describe('Login Test', () => {
  it('Failure', () => {
    cy.visit(base_url);
    cy.get('#username').type('user');
        cy.get('#loginButton').click().should(() => {
            expect(localStorage.getItem('isAuth')).to.eq('false')
          })
        expect(cy.get('#logout').should('not.exist'));
  });
});

describe('Login Test', () => {
  it('Failure', () => {
    cy.visit(base_url);
    cy.get('#password').type('12345');
    cy.get('#loginButton').click().should(() => {
        expect(localStorage.getItem('isAuth')).to.eq('false')
      })
    expect(cy.get('#logout').should('not.exist'));
  });
});

describe('Login Test', () => {
  it('Successful', () => {
    cy.visit(base_url);
    cy.get('#username').type('user');
    cy.get('#password').type('12345');
    cy.get('#loginButton').click().should(() => {
        expect(localStorage.getItem('isAuth')).to.eq('true')
      })
    expect(cy.get('#logout').should('exist'));
  });
});

describe('Login Test', () => {
  it('Login and Logout', () => {
    cy.visit(base_url);
    cy.get('#username').type('user');
    cy.get('#password').type('12345');
    cy.get('#loginButton').click().should(() => {
        expect(localStorage.getItem('isAuth')).to.eq('true')
      })
    expect(cy.get('#logout').should('exist'));
    cy.get('#logout').click().should(() => {
        expect(localStorage.getItem('isAuth')).to.eq('false')
      })
    expect(cy.get('#logout').should('not.exist'));
  });
});