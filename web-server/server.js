const {Pool, Client} = require('pg');
const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = process.env.PORT || 5000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// const pool = new Pool({
//   user: 'postgres',
//   host: 'twitterbot.ccptai2yvrbu.us-east-2.rds.amazonaws.com',
//   database: 'twitterbot',
//   password: 'gertrudes123',
//   port: 5432,
// })

// pool.query('SELECT NOW()', (err, res) => {
//   console.log(err, res)
//   pool.end()
// })

const client = new Client({
  user: 'postgres',
  host: 'twitterbot.ccptai2yvrbu.us-east-2.rds.amazonaws.com',
  database: 'twitterbot',
  password: 'gertrudes123',
  port: 5432,
});

client.connect(err => {
  if (err) {
    console.error('connection error', err.stack)
  } else {
    console.log('connected')
  }
})

app.get('/api/comments/0', (req, res) => {
  client.query('SELECT text, classification FROM tweets WHERE classification = 0', (error, result) => {
    if (error) {
      console.log(error.stack);
    } else {
      res.send({count: result.rows.length, comments: result.rows});
    }
  })
});

app.get('/api/comments/1', (req, res) => {
  client.query('SELECT text, classification FROM tweets WHERE classification = 1', (error, result) => {
    if (error) {
      console.log(error.stack);
    } else {
      res.send({count: result.rows.length, comments: result.rows});
    }
  })
});

app.get('/api/count/0', (req, res) => {
  client.query('SELECT count(classification) FROM tweets WHERE classification = 0', (error, result) => {
    if (error) {
      console.log(error.stack);
    } else {
      res.send({count: result.rows[0].count});
    }
  })
});

app.get('/api/count/1', (req, res) => {
  client.query('SELECT count(classification) FROM tweets WHERE classification = 1', (error, result) => {
    if (error) {
      console.log(error.stack);
    } else {
      res.send({count: result.rows[0].count});
    }
  })
});

app.post('/api/login', (req, res) => {
  if(req.body.username === 'user' && req.body.password === '12345'){ 
    res.send( { isAuth: true } );
  }
  else{
    res.send({ isAuth: false });
  }
});

app.listen(port, () => console.log(`Listening on port ${port}`));