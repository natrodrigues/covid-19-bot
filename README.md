# Projeto COVID-BOT

## Grupo 6 da disciplina ES - 1s2020

Bot que dispara no Twitter informações coletadas em diversos sites de noticias nacionais e estrangeiros sobre o Corona Vírus.

Vamos projetar Web Crawlers para coletar informações de sites que seram formatados em tweets através de requisições HTTP (get/post/delete) utilizando a API do Twitter.

## Instalação

Antes de mais nada, devemos ter o Python3 instalado. Pra quem usa distros Linux baseada em Ubuntu:

`sudo apt-get update && sudo apt-get install python3.8`

Para usarmos virtualenv no Python:

`python3.8 -m venv venv && source venv/bin/activate`

Instalação do Poetry como gerenciador de dependências:

`pip3 install poetry`

Para termos a disposição as dependências, execute:

`poetry install`

## Gerar o json com manchetes e endereço

Para gerar um arquivo (nyt.json) com um array de objetos com `title` e `link`, basta rodar a linha de comando

`scrapy crawl nyt -o nyt.json`

## Acesso à API do Twitter

O acesso às APIs do Twitter requer um conjunto de credenciais que você deve passar a cada solicitação. O contexto do usuário requer uma chave (``CONSUMER_KEY``) e um segredo (``CONSUMER_SECRET``) da API e um conjunto de tokens de acesso específicos (``ACCESS_TOKEN`` e ``ACCESS_TOKEN_SECRET``) ao usuário. Essas chaves e tokens são geradas pelo app assim que sua conta de desenvolvedor é aprovada pelo sistema.

### Fazer login

Para realizar o login é necessário passar as credenciais pelo ``TwitterPoster`` que já chama a função ``__init__``:

```
poster = TwitterPoster(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
```

Que serão gerenciadas por:

```
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
    self._api = tweepy.API(auth, wait_on_rate_limit=True)
```

### Para realizar a postagem

Para twitar deve-se passar um texto e uma url pelo método ``post`` de ``TwitterPoster``:
```
text = PostManager.readJson()['title'];
url = PostManager.readJson()['link'];
res = poster.post(text, url);
```


## Arquitetura

A seguir, a descrição do [diagrama](https://drive.google.com/file/d/1t67Atdq32e8gMtYqBQoBATMr51UNVTSg/view).

O principal modelo de arquitetura adotado é o de Arquitetura Modelo-Visão-Controle (MVC). Separa-se então as camadas:
- Modelo, que gerencia a execução do projeto através das funções construídas com base nas regras de negócio da aplicação;
- Visão, i.e. a saída dos dados para exibição;
- Controle, entrada e coleta dos dados.

O Shared Repository é usado para manter o contato entre o repositório de comentários brutos e o padrão Observer da busca de comentários. Também para manter o contato entre esse repositório, o processamento de dados e o repositório de dados classificados.

### Principais Componentes
#### Modelo
- #### Busca de Comentário:
    Encontra e reabastece o banco de dados com todas e quaisquer menções e tags incluídas no filtro de buscas, sem separação ou alteração.

- #### API Web:
    Aplicação integrada da rede social que facilita a interação com o programa na busca e tratamento de comentários. Junto da Busca de Comentário, faz parte do padrão Observer.

- #### Processamento de Comentários:
    O processamento conta com a junção de classificações internas dos tipos de comentários a serem analisados (críticas construtivas, reclamações, avisos, etc...) para serem distribuídos entre as suas respectivas tabelas no Rep. de Comentários Classificados

- #### Gerenciador Repositório-Controlador:
    Faz a comunicação entre o controlador e o BD acessando o armazenamento de dados tratados. Junto do Processamento de comentários, os bancos e os repositórios


#### Visão
- #### GUI Visão MVC:
    Interface gráfica que mostra ao usuário os dados coletados e tratados.


#### Controlador
- #### Controlador MVC:
    Gerencia as ações da interface gráfica de acordo com as necessidades e buscas do usuário no banco de dados