from distutils.core import setup

setup(
    name = 'TwitterBot',
    version = '1.0',
    description = 'A web crawler and text analyser bot for Twitter mentions',
    author= 'Grupo 6',
    package_dir={'':'src'},
    packages=['comments_system', 'modules'],
    requires=['nltk', 'sklearn', 'joblib', 'pandas'],
)